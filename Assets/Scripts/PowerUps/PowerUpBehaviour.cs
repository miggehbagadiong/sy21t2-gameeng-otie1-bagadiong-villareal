using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpBehaviour : MonoBehaviour
{   //Activate and interact

    [SerializeField] private PowerUp powerUp;

    public PowerUpController PowerUpController;

    private Transform TransformPowUp;
    void Awake()
    {
        TransformPowUp = transform;
    }

    private void OnTriggerEnter(Collider other)
    {   //when collided with player activate powerup
        if (other.gameObject.CompareTag("Player"))
        {
            ActivatePowUp();
            gameObject.SetActive(false);
        }
    }

    private void ActivatePowUp()
    {
        PowerUpController.StartPowerUp(powerUp);
    }

    public void SetPowUp(PowerUp powUp)
    {
        this.powerUp = powUp;
        gameObject.name = powUp.namePowUp;
    }
}

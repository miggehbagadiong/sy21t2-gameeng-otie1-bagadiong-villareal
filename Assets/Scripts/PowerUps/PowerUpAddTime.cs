using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using TMPro;
using UnityEngine.UIElements;

public class PowerUpAddTime : MonoBehaviour
{
    public TimeManager TimeManager;
    public bool isObtained = false;
    
    private float addedTime = 10.0f;
    private AudioManager audioMgr;

    void Start()
    {
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            isObtained = true;
            TimeManager = SingletonManager.Get<TimeManager>();
            TimeManager.AddTime(addedTime);
            audioMgr.Play("AddTime");
            Destroy(gameObject);
        }

    }
    
}

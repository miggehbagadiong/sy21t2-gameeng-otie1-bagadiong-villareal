using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class PowerUp
{
    [SerializeField] public string namePowUp;
    public float durationPowUp;
    
    //
    public enum PowerUpType{Speed, AddTime}

    public PowerUpType Type;
    //
    
    //Unity events
    public UnityEvent startEffect;
    public UnityEvent endEffect;

    public void Start()
    {
        if (startEffect != null)
        {
            startEffect.Invoke();
        }
    }
    public void Fin()
    {
        if (endEffect != null)
        {
            endEffect.Invoke();
        }
    }
}

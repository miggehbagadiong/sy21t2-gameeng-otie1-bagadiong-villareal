using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpController : MonoBehaviour
{
    public GameObject PowUpPrefab;
    //spawnable powerups
    public List<PowerUp> PowerUps;

    public Dictionary<PowerUp, float> activePowUps = new Dictionary<PowerUp, float>();
    public GameObject[] Locations;
    
    private List<PowerUp> powUpKeys = new List<PowerUp>();
    
    void Awake()
    {
        for (int i = 0; i < Locations.Length; i++)
        {
            SpawnPowUpObj(PowerUps[0], Locations[i]);
        }
    }

    void Update()
    {
        ActivePowUpsHandler();
    }

    public void ActivePowUpsHandler()
    {
        //check if theres any update/changes in the dictionary; any new powerups added
        bool Update = false;
        
        if (activePowUps.Count > 0)
        {   
            foreach (PowerUp powerUp in powUpKeys)
            {   //timer
                if (activePowUps[powerUp] > 0)  //if there is still time
                {
                    activePowUps[powerUp] -= Time.deltaTime;
                }
                else  //if not time left; run out of time
                {
                    Update = true;
                    activePowUps.Remove(powerUp);
                    powerUp.Fin();
                }
            }
        }

        if (Update)
        {   //refresh keys
            powUpKeys = new List<PowerUp>(activePowUps.Keys);
        }

    }

    public void StartPowerUp(PowerUp powerUp)
    {
        // check if contains a key (dictionary) - Check if same power up is being applied currently
        // if true = stack or add duration 
        if (!activePowUps.ContainsKey(powerUp))
        {
            powerUp.Start();
            activePowUps.Add(powerUp, powerUp.durationPowUp);
        }
        else
        {   //effect stack
            activePowUps[powerUp] += powerUp.durationPowUp;
        }

        powUpKeys = new List<PowerUp>(activePowUps.Keys);
    }

    public GameObject SpawnPowUpObj(PowerUp powerUp, GameObject loc)
    {
        //GameObject PowUpGo = Instantiate(PowUpPrefab);
        GameObject PowUpGo = GameObject.Instantiate(PowUpPrefab, loc.transform.position,Quaternion.identity);
        var PowUpBehaviour = PowUpGo.GetComponent<PowerUpBehaviour>();
        PowUpBehaviour.PowerUpController = this;
        PowUpBehaviour.SetPowUp(powerUp);
        //PowUpGo.transform.position = pos;
        return PowUpGo;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpeed : MonoBehaviour
{   //Increase speed of player and the animals that are unlocked by the player for a short time
    // get player and animal speed
    public Movement PlayerMovement;

    public GameObject icon;
    //public AnimalAi AnimalAi;

    private AudioManager audioMgr;
    
    void Start()
    {
        icon.SetActive(false);
        //AnimalAi = SingletonManager.Get<AnimalAi>();
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
    }

    public void StartIncreaseSpeed()
    {
        PlayerMovement.Speed *= 2.0f;
        icon.SetActive(true);
        //AnimalAi.Agent.speed *= 2.0f;
        audioMgr.Play("DashPowerupActivated");
    }

    public void EndIncreaseSpeed()
    {
        PlayerMovement.Speed /= 2.0f;
        icon.SetActive(false);
        //AnimalAi.Agent.speed /= 2.0f;
        audioMgr.Play("DashPowerupDeactivated");
    }
}

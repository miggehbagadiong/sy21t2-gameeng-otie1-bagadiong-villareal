using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.AI;
using Random = UnityEngine.Random;

public class EnemyAi : MonoBehaviour
{
   public float lookRad = 5f;
   
   Transform target;
   NavMeshAgent agent;
   
   public PlayerManager PlayerManager;

   void Start()
   {
      target = SingletonManager.Get<PlayerManager>().Player.transform;
      agent = GetComponent<NavMeshAgent>();
      
      PlayerManager = SingletonManager.Get<PlayerManager>();
    
   }

   void Update()
   {
      float dist = Vector3.Distance(target.position, transform.position);

      if (dist <= lookRad)
      {
         agent.SetDestination(target.position);

         if (dist <= agent.stoppingDistance)
         {
            // Attack Target or Capture Target
            LookAtTarget();
            
         }
      }
   }

   #region Ai Follow Player
   void LookAtTarget()
   {
      Vector3 dir = (target.position - transform.position).normalized;
      Quaternion lookRot = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.z));
      transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * 5.0f);
   }
   
   private void OnDrawGizmosSelected()
   {
      Gizmos.color = Color.red;
      Gizmos.DrawWireSphere(transform.position, lookRad);
   }

  
   #endregion
   
   private void OnCollisionEnter(Collision other)
   {
      if (other.gameObject.CompareTag("Player"))
      {
         Debug.Log("Collided with Player");

         if (PlayerManager.AnimalPlayerStorage.animalList.Count == 0)
         {
            PlayerManager.AnimalPlayerStorage.isEmpty = true;
         }
         
      }

   }

   private void OnTriggerEnter(Collider other)
   {
      if (other.gameObject.CompareTag("Cage"))
      {
         agent.isStopped = true;
      }
   }
}

/*
 * Reference for the Code:
 * https://www.youtube.com/watch?v=xppompv1DBg&t=12s
 * 
 */
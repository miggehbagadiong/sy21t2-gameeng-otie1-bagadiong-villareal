using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalManager : MonoBehaviour
{ //anything to be passed by reference here
    
    public GameObject[] Animal;
    
    public bool isReleased = false;
   
    public AnimalSpawner AnimSpawner;
    public AnimalAi AnimAi;
    public Animal Anim;
    public Animal[] Animals;
    
    public List<Animal.AnimalTier> AnimalList;


    private void Awake()
    {
        SingletonManager.Register(this);
    }
    
    
}

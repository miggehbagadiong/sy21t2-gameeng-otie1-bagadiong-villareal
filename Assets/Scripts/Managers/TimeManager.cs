using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimeManager : MonoBehaviour
{
    
    private SceneController sceneController;
    private PlayerManager player;
    private AudioManager audioMgr;

    public float AllotedTime = 60; //60

    public bool isTimerRunning = true;
    
    void Start()
    {
        // reference to other scripts via singleton
        sceneController = SingletonManager.Get<SceneController>();
        player = SingletonManager.Get<PlayerManager>();
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        
        SingletonManager.Register(this);

    }

    public IEnumerator Timer()
    {
        GameObject.Find("Player").GetComponent<Movement>().enabled = false;
        
        yield return new WaitForSeconds(1.0f); // delay for the time to start (2.0)
        
        GameObject.Find("Player").GetComponent<Movement>().enabled = true;
        
        float ZeroTime = 0;

        while (ZeroTime <= AllotedTime) 
        {
            
            if (isTimerRunning == true)
                AllotedTime -= Time.deltaTime;
            
            // When time reaches 0
           if (AllotedTime <= 0)
           {
                sceneController.MoveToGameOver();
                audioMgr.Play("PlayerKilled");
                GameObject.Find("Player").GetComponent<Movement>().enabled = false; // disable player when game is over
           }

            yield return null;
        }
        
    }
    
    public void OnStartTimerPressed()
    {
        // Start timer coroutine when the start game button is pressed
        StartCoroutine(Timer());
    }

    public float GetAllotedTime()
    {
        return AllotedTime;
    }

    public float AddTime(float increment)
    {
        AllotedTime += increment;

        return AllotedTime;
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class SceneController : MonoBehaviour
{
    // Canvas Scenes
    public Canvas StartMenuCanvas;
    public Canvas TutorialCanvas;
    public Canvas GameUiCanvas;
    public Canvas GameOverCanvas;
    public Canvas GameWinCanvas;

    private AudioManager audioMgr;
    
    private void Awake()
    {
        ShowStartScreen();
    }

    void Start()
    {
        SingletonManager.Register(this);

        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        
    }

    #region ButtonInteractions

    public void OnStartButtonPressed()
    {
        ShowTutorialScreen();
    }

    public void OnStartGameButtonPressed()
    {
        StartGame();
        audioMgr.Play("GameBGM");
    }

    public void OnExitButtonPressed()
    {
        ExitGame();
    }

    public void OnRestartGameButtonPressed()
    {
        RestartGame();
    }

    public void OnTryAgainButtonPressed()
    {
        ShowTryAgainScreen();
    }

    #endregion
    
    #region Scene Commands

    public void ShowStartScreen()
    {
        StartMenuCanvas.enabled = true;
        TutorialCanvas.enabled = false;
        GameUiCanvas.enabled = false;
        GameOverCanvas.enabled = false;
        GameWinCanvas.enabled = false;
    }

    public void ShowTryAgainScreen()
    {
        StartMenuCanvas.enabled = true;
        TutorialCanvas.enabled = false;
        GameUiCanvas.enabled = false;
        GameOverCanvas.enabled = false;
        GameWinCanvas.enabled = false;
    }
    
    public void ShowTutorialScreen()
    {
        StartMenuCanvas.enabled = false;
        TutorialCanvas.enabled = true;
        GameUiCanvas.enabled = false;
        GameOverCanvas.enabled = false;
        GameWinCanvas.enabled = false;
    }
    
    public void StartGame()
    {
        StartMenuCanvas.enabled = false;
        TutorialCanvas.enabled = false;
        GameUiCanvas.enabled = true;
        GameOverCanvas.enabled = false;
        GameWinCanvas.enabled = false;
    }
    public void MoveToGameOver()
    {
        Debug.Log("Game is Over!!");
        StartMenuCanvas.enabled = false;
        TutorialCanvas.enabled = false;
        GameUiCanvas.enabled = false;
        GameOverCanvas.enabled = true;
        GameWinCanvas.enabled = false;
    }

    public void MoveToWin()
    {
        Debug.Log("Successfully Escaped!");
        StartMenuCanvas.enabled = false;
        TutorialCanvas.enabled = false;
        GameUiCanvas.enabled = false;
        GameOverCanvas.enabled = false;
        GameWinCanvas.enabled = true;
    }

    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    
    public void ExitGame()
    {
        Debug.Log("Quit Game!");
        Application.Quit();
    }

    #endregion
    
}

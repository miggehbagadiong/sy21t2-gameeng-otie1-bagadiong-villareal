using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public int Score { get; private set; } = 0;

    private void Awake()
    {
        SingletonManager.Register(this);
    }

    public void AddScore(int value) //make specific score to add depending on what tier the animal is captured
    {
        Score += value;

    }

    public void DeductScore(int value)
    {
        Score -= value;
    }
    
}

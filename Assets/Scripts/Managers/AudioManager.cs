using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    void Awake()
    {
        foreach (Sound s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    void Start()
    {
        SingletonManager.Register(this);
    }

    public void Play(string audioName)
    {
        Sound s = Array.Find(sounds, sound => sound.name == audioName);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + audioName + " not found!");
            return;
        }
        
        s.source.Play();
    }
    
    public void Stop(string audioName)
    {
        Sound s = Array.Find(sounds, sound => sound.name == audioName);

        if (s == null)
        {
            Debug.LogWarning("Sound: " + audioName + " not found!");
            return;
        }
        
        s.source.Stop();
    }
}

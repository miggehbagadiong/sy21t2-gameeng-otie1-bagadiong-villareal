using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Serialization;

public class AnimalAi : MonoBehaviour
{
    
    public float LookRad = 3.0f;
    public bool IsReleased = false;
    
    public NavMeshAgent Agent;
    Transform Player;

    void Start()
    {
        //SingletonManager.Register(this);
        
        Player = SingletonManager.Get<PlayerManager>().Player.transform;
        Agent = GetComponent<NavMeshAgent>();
    }
    
    void Update()
    {
        //when released from cage
        if (this.IsReleased)
        {
            float dist = Vector3.Distance(Player.position, transform.position);
        
            if (dist <= LookRad)
            {
                Agent.SetDestination(Player.position);
                
                if (dist <= Agent.stoppingDistance)
                {
                    LookAtTarget();
                }
            }
        }
        
    }

    #region Animal Ai follow Player

    void LookAtTarget()
    {
        Vector3 dir = (Player.position - transform.position).normalized;
        Quaternion lookRot = Quaternion.LookRotation(new Vector3(dir.x, 0, dir.y));
        transform.rotation = Quaternion.Slerp(transform.rotation, lookRot, Time.deltaTime * 5.0f);
    }
    

    #endregion
    
}

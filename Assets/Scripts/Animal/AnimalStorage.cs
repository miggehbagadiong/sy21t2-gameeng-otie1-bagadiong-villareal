using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Events;

public class AnimalStorage : MonoBehaviour
{
    public UnityEvent<Animal> EvtAnimalAcquired { get; set; } = new();
    public UnityEvent<Animal> EvtAnimalRemoved { get; set; } = new();
    
    public List<Animal> AnimalList => animalList;

    public List<Animal> animalList = new();
    public bool isEmpty = true;
    //private bool justCollided = false;

    [HideInInspector] public bool isDead = false;

    //Scene controller cast
    private SceneController sceneControl;

    private TimeManager timer;
    
    // Cast to Audio Manager
    private AudioManager audioMgr;

    private void Awake()
    {
        animalList = new List<Animal>(); //
        SingletonManager.Register(this);
    }

    void Start()
    {
        sceneControl = SingletonManager.Get<SceneController>();
        timer = GameObject.Find("Time Manager").GetComponent<TimeManager>();
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
    }

    public void AddAnimal(Animal animal)
    {
        animalList.Add(animal);
        animal.AddTierValueToScore();
        EvtAnimalAcquired.Invoke(animal);
        print("Added Animal: " + animal);
        //print("Animals left :" + animalList.Count);
    }

    public void RemoveAnimal(Animal animal)
    {
        animalList.Remove(animal);
        //animal.CapturedByEnemy(animal);
        animal.DeductTierValueToScore();
        EvtAnimalRemoved.Invoke(animal);
        print("Removed Animal: " + animal);
    }

    private void OnCollisionEnter(Collision other)
    {
        //when collided to enemy
        AnimalManager animal = SingletonManager.Get<AnimalManager>();
        print("on collision enter");
        if (other.gameObject.CompareTag("Enemy"))
        { 
            print("collided with enemy");
            if (!isEmpty)  // (animal != null) ; 
            {
                // Remove the animal at the last index of the list
                animalList.Last().CapturedByEnemy();
                RemoveAnimal(animalList.Last());
                audioMgr.Play("ReduceAnimals");
            }
            else
            {
                    this.gameObject.SetActive(false);
                    print("No Animals to capture");
                    sceneControl.MoveToGameOver(); // When captured show the game over ui
                    audioMgr.Play("PlayerKilled");
                    audioMgr.Stop("GameBGM");
                    timer.isTimerRunning = false;
            }
        }
    }

    public IEnumerator RemoveAnimalInterval(float delay)
    {
        yield return new WaitForSeconds(delay);
        //justCollided = false;
    }
    
}

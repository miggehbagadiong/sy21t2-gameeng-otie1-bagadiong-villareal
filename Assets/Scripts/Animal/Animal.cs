using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Animal : MonoBehaviour
{   
    //isReleased
    public enum AnimalTier{R, SR, SSR} //rarity

    public enum AnimalTierValue
    {
        Rvalue = 100,
        SrValue = 300,
        SsrValue = 500,
    }

    public AnimalTier Tier;
    public AnimalTierValue TierValue;
    
    public static bool IsCapturedByEnemy { get;  set; } = false;
    
    [SerializeField] private Animal.AnimalTier animalTier;
    [SerializeField] private Animal.AnimalTierValue animalTierValue;
    
    // void start

    public Animal.AnimalTier GetAnimalTier() { return animalTier; }

    public Animal.AnimalTierValue GetAnimalValue() { return animalTierValue; }

    public void CapturedByEnemy()
    {
        if (gameObject != null)
        {
            Destroy(this.gameObject);
            //Destroy(animal.gameObject);
            print((this.gameObject.name + ". "));
        }
    }
    
    //conditions for each tiers = to be added to players score
    public void AddTierValueToScore()
    {
        ScoreManager scoreManager = SingletonManager.Get<ScoreManager>();
        
        if (TierValue == AnimalTierValue.Rvalue)
        {
            scoreManager.AddScore((int) AnimalTierValue.Rvalue);

        } else if (TierValue == AnimalTierValue.SrValue)
        {
            scoreManager.AddScore((int) AnimalTierValue.SrValue);

        }else if (TierValue == AnimalTierValue.SsrValue)
        {
            scoreManager.AddScore((int) AnimalTierValue.SsrValue);

        }
    }
    
    public void DeductTierValueToScore()
    {
        ScoreManager scoreManager = SingletonManager.Get<ScoreManager>();
        
        if (TierValue == AnimalTierValue.Rvalue)
        {
            scoreManager.DeductScore((int) AnimalTierValue.Rvalue);
        } else if (TierValue == AnimalTierValue.SrValue)
        {
            scoreManager.DeductScore((int) AnimalTierValue.SrValue);
        }else if (TierValue == AnimalTierValue.SsrValue)
        {
            scoreManager.DeductScore((int) AnimalTierValue.SsrValue);
        }
    }
}

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class EscapeGate : MonoBehaviour
{
    public SceneController sceneController;
    public AnimalStorage AnimalStorage;

    private AudioManager audioMgr;
    private TimeManager timer;

    void Start()
    {
        timer = GameObject.Find("Time Manager").GetComponent<TimeManager>();
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
        AnimalStorage = SingletonManager.Get<AnimalStorage>();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            //set condition to 
            // if timer < 0 && if cage counter >= 1 or animal storage >= 1 => win canvas
            // else => game over canvas
            
            CheckWinConditions();
            timer.isTimerRunning = false;

        }
    }

    void CheckWinConditions()
    {
        if (timer.GetAllotedTime() > 0) //&& if cage counter >= 1 or animal storage >= 1
        {
            //show win canvas
            if (!AnimalStorage.isEmpty)
            {
                sceneController.MoveToWin();
                audioMgr.Play("YouWin");
                AnimalStorage.gameObject.SetActive(false);
                audioMgr.Stop("GameBGM");
            }
            else
            {
                sceneController.MoveToGameOver();
                audioMgr.Play("PlayerKilled");
                AnimalStorage.gameObject.SetActive(false);
                audioMgr.Stop("GameBGM");
            }
            
        }
        else
        {
            //show gameover canvas
            sceneController.MoveToGameOver();
        }
    }
}

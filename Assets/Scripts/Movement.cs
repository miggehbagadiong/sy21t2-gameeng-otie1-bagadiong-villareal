using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class Movement : MonoBehaviour
{
    public float Speed;
    public float Rot;
    public Transform Target;

    private Rigidbody rb;
    private Vector3 movement;

    private void Awake()
    {
        TryGetComponent(out rb);
    }
    
    private void FixedUpdate()
    {
        MovePlayer();
    }

    void MovePlayer()
    {
        movement = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));

        rb.position += movement * Speed * Time.deltaTime;

        if (movement.sqrMagnitude > 0)
        {
            Quaternion rot = Quaternion.LookRotation(movement, Vector3.up);
            rb.rotation = Quaternion.Lerp(rb.rotation, rot, Time.deltaTime * this.Rot);
        }

    }

}

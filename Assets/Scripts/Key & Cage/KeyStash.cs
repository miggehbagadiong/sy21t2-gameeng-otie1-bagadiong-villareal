using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class KeyStash : MonoBehaviour
{
    public UnityEvent<Key.KeyType> EvtKeyAcquired { get; set; } = new(); //property
    public UnityEvent<Key.KeyType> EvtKeyRemoved { get; set; } = new();

    [Header("References")]
    public Key[] KeyPrefabs;
    public List<Key.KeyType> KeyList => keyList; //getter property
    
    private List<Key.KeyType> keyList = new();

    // Audio SFX
    private AudioManager audioMgr;
    
    
    private void Awake()
    {
        keyList = new List<Key.KeyType>();
    }

    private void Start()
    {
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();
    }

    public void AddKey(Key key)
    {
        keyList.Add(key.Type);
        
        EvtKeyAcquired.Invoke(key.Type);
        
        print("Added Key" + key);
    }

    public void RemoveKey(Key.KeyType type)
    {
        keyList.Remove(type);
        
        EvtKeyRemoved.Invoke(type);
        
        print("Removed Key" + type);
    }

    public bool KeysCollected(Key.KeyType KeyColor) { return keyList.Contains(KeyColor); }

    private void OnTriggerEnter(Collider other)
    { 
        // Check if Collided with key
        Key key = other.GetComponent<Key>();
        // Check if Collided with Cage
        ColoredCage colorCage = other.GetComponent<ColoredCage>();

        //** Might change to press key to pick up **//
        
        // When collided with key
        if (key != null)
        {
            AddKey(key);
            Destroy(key.gameObject);
            audioMgr.Play("CollectedKey");
        }
        
        //** Might change to press key to open cage **//
        
        // When Collided with Cage
        if (colorCage != null)
        {
            print("CAGE DETECTED");
            // When Key Color and Cage Color match 
            if (KeysCollected(colorCage.KeyColor))
            {
                RemoveKey(colorCage.KeyColor);
                colorCage.OpenCage();
            }
        }
    }

    
}

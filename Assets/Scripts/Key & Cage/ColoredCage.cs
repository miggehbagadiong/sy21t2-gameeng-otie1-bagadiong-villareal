using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColoredCage : MonoBehaviour
{
    public Key.KeyType KeyColor;
    public Transform SpawnPoint;
    public int OpenedCageCount = 0;
    public Animal Animal { get; set; }  //property
    public bool IsOpen { get; private set; } = false;
    
    // get singleton instances
    private PlayerManager player;
    private AudioManager audioMgr;
    
    private void Start()
    {
        player = SingletonManager.Get<PlayerManager>();
        audioMgr = GameObject.Find("Audio Manager").GetComponent<AudioManager>();

    }

    public void OpenCage()
    {
        IsOpen = true;
        gameObject.SetActive(false);
        OpenedCageCount++;
        
        //if statement - check if its animal

        Animal.GetComponent<AnimalAi>().IsReleased = true;
        //animAi.IsReleased = true;
        player.AnimalPlayerStorage.AddAnimal(Animal);
        player.AnimalPlayerStorage.isEmpty = false;
        audioMgr.Play("CageOpened");
       

    }
    
}

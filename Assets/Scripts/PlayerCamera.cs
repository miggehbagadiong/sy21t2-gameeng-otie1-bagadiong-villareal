using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    public Camera Camera;

    void Update()
    {
        if (Camera != null)
        {
            Vector3 direction = (Vector3.up * 2 + Vector3.back)*3;
            
            Camera.transform.position = transform.position + direction;
            
            Camera.transform.LookAt(transform.position);
        }
    }
}

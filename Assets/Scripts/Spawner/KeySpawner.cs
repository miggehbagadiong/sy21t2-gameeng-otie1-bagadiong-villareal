using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.Serialization;


public class KeySpawner : MonoBehaviour
{
    public GameObject[] Keys;
    public GameObject[] Locations;

    public List<int> LocationList = new List<int>(); //make it GameObject
    private int rndNum;
    void Start()
    {
        LocationList = new List<int>(new int[Locations.Length]);

        for (int i = 0; i < Locations.Length; i++)
        {
            rndNum = Random.Range(1, (Keys.Length)+1);
            while (LocationList.Contains(rndNum))
            {
                rndNum = Random.Range(1, (Keys.Length)+1);
            }
            
            LocationList[i] = rndNum;
            //Locations[i] = Keys[(LocationList[i] - 1)];
            
            GameObject spawn = GameObject.Instantiate(Keys[i], Locations[rndNum-1].transform.position,
                Quaternion.identity);
            
            
        }
        
        // for (int i = 0; i < Locations.Length; i++)
        // {
        //     GameObject spawn = GameObject.Instantiate(Keys[i], Locations[i].transform.position,
        //         Quaternion.identity);
        //     
        // }    
    }
 
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpTimerSpawner : MonoBehaviour
{
    public GameObject PowerUp;
    public GameObject[] Locations;

    void Start()
    {
        for (int i = 0; i < Locations.Length; i++)
        {

            //int rndIndex = Random.Range(0, PowerUps.Length);
            GameObject spawn = GameObject.Instantiate(PowerUp, Locations[i].transform.position,
                Quaternion.identity);
            
            // var PowUpBehaviour = spawn.GetComponent<PowerUpBehaviour>();
            // PowUpBehaviour.PowerUpController = this;
            
        }
    }
    
    // public GameObject SpawnPowUpObj(PowerUp powerUp, Vector3 pos)
    // {
    //     GameObject PowUpGo = Instantiate(PowUpPrefab);
    //     var PowUpBehaviour = PowUpGo.GetComponent<PowerUpBehaviour>();
    //     PowUpBehaviour.PowerUpController = this;
    //     PowUpBehaviour.SetPowUp(powerUp);
    //     PowUpGo.transform.position = pos;
    //     return PowUpGo;
    // }
    
}

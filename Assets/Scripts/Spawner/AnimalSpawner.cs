using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimalSpawner : MonoBehaviour
{
    public GameObject[] Animals;
    
    void Start()
    {
        LevelManager levelManager = SingletonManager.Get<LevelManager>();
        
        for (int i = 0; i < levelManager.Cage.Length; i++) 
        {
            int rndIndex = Random.Range(0, Animals.Length);
            GameObject spawn = GameObject.Instantiate(Animals[rndIndex], levelManager.Cage[i].SpawnPoint.position, 
                Quaternion.identity);
            levelManager.Cage[i].Animal = spawn.GetComponent<Animal>();
        }
    }
    
}

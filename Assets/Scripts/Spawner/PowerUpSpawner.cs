using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

public class PowerUpSpawner : MonoBehaviour
{
    public GameObject PowerUp;
    public GameObject[] Locations;
    
    void Start()
    {
        for (int i = 0; i < Locations.Length; i++)
        {

            //int rndIndex = Random.Range(0, PowerUps.Length);
            GameObject spawn = GameObject.Instantiate(PowerUp, Locations[i].transform.position,
                Quaternion.identity);
        }
    }
    
    

   
}

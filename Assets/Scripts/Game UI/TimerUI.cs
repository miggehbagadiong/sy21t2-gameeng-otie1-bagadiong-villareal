using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TimerUI : MonoBehaviour
{
    public TextMeshProUGUI TimerText;
    public TextMeshProUGUI AddTimeText;
    public PowerUpAddTime AddTime;
    
    private TimeManager timeManager;
    private float timer = 3;
    
    void Start()
    {
        //timeManager = SingletonManager.Get<TimeManager>();
        //AddTime = SingletonManager.Get<PowerUpAddTime>();
        AddTime = GetComponent<PowerUpAddTime>();
        timeManager = GetComponent<TimeManager>();
        AddTimeText.enabled = false;
    }

    // Update is called once per frame
    void Update()
    {
        var timerVal = (int)timeManager.GetAllotedTime();
        TimerText.text = timerVal.ToString();

        //TimerText.text = timeManager.AllotedTime.ToString();
        
        ShowAddedTime();
        
        
    }

    void ShowAddedTime()
    {
        //check in PowerUpAddTime if isObtained show AddTimeText;
        if (AddTime.isObtained)
        {
            AddTimeText.enabled = true;
            //start timer countdown 
            timer -= Time.deltaTime;
            //time reaches to 0 set to false
            if (timer <= 0)
            {
                AddTimeText.enabled = false;
                AddTime.isObtained = false;
            }
        }
    }
    
}

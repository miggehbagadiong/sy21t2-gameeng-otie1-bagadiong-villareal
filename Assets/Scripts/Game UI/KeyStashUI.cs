using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KeyStashUI : MonoBehaviour
{
    [Header("Configuration")]
    public GameObject ElementPrefab;

    [Header("References")]
    public KeyStash KeyStash;    
    public Transform KeyContainer;

    private Dictionary<Key.KeyType, GameObject> keys = new();

    private void Start() 
    {
        KeyStash.EvtKeyAcquired.AddListener(OnKeyAcquired);
        KeyStash.EvtKeyRemoved.AddListener(OnKeyRemoved);

        for (int i = 0; i < KeyStash.KeyPrefabs.Length; i++)
        {
            Key key = KeyStash.KeyPrefabs[i];

            GameObject newKey = Instantiate(ElementPrefab, KeyContainer);

            Color newColor = key.Color;
            newColor.a = 0.4f;
            newKey.GetComponent<Image>().color = newColor;

            keys.Add(key.Type, newKey);
        }
    }

    private void OnKeyAcquired(Key.KeyType type)
    {
        Color color = keys[type].GetComponent<Image>().color;
        color.a = 1.0f;
        keys[type].GetComponent<Image>().color = color;
    }
    
    private void OnKeyRemoved(Key.KeyType type)
    {
        Color color = keys[type].GetComponent<Image>().color;
        color.a = 0.4f;
        keys[type].GetComponent<Image>().color = color;
    }
}

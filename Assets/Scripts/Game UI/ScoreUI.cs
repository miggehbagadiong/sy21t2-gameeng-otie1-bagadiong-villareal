using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.Serialization;

public class ScoreUI : MonoBehaviour
{
    public TextMeshProUGUI ScoreTxt;
    public TextMeshProUGUI youLoseScoreTxt;
    public TextMeshProUGUI youWinScoreText;
    
    // Access to ScoreManager
    private ScoreManager scoreManager;
    
    void Start()
    {
        scoreManager = SingletonManager.Get<ScoreManager>();
        ScoreTxt.text = scoreManager.Score.ToString();
    }
    
    void Update()
    {
        // Update Score in game canvas
        ScoreTxt.text = scoreManager.Score.ToString();
        
        //Update score on you lose canvas
        youLoseScoreTxt.text = scoreManager.Score.ToString();
        
        //Update score on you win canvas
        youWinScoreText.text = scoreManager.Score.ToString();
    }
    
}
